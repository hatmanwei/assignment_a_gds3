This project uses Ben Esposito's First Person Drift Controller: http://torahhorse.com/first-person-drifter-controller-for-unity3d

Low Poly Style - Tree Pack, Agile Reaction, Unity Asset Store

Realistic Tree 12, Rakshi Games, Unity Asset Store

Bark Textures PBR - Volume One, A dog's life software, Unity Asset Store

Breakable Floor Tiles, TheFolgore, Unity Asset Store

MK Glass Free, Michael Kremmel, Unity Asset Store

Skybox, Clod, Unity Asset Store

black_reads.png, Google Image, <http://www.loughboora.com/wp-content/themes/boora/images/black_reads.png

Post Processing Stack, Unity Technologies, Unity Asset Store

ProBuilder, Unity Technologies, Unity Asset Store

Old Kitchen Assets, HoSchu 3D, Unity Asset Store